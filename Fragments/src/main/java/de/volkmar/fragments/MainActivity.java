package de.volkmar.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends FragmentActivity {

    public static final String BUNDLE_NAME_KEY = "Name";

    private FragmentOne fragmentOne;
    private FragmentTwo fragmentTwo;

    private int iCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle bundleOne = new Bundle();
        bundleOne.putString(BUNDLE_NAME_KEY, "Eins!");

        Bundle bundleTwo = new Bundle();
        bundleTwo.putString(BUNDLE_NAME_KEY, "Zwei!");

        fragmentOne = (FragmentOne) Fragment.instantiate(this, FragmentOne.class.getName(), bundleOne);
        fragmentTwo = (FragmentTwo) Fragment.instantiate(this, FragmentTwo.class.getName(), bundleTwo);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        fragmentTransaction.add(R.id.flFragmentContainer, fragmentOne);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_switch) {

            FragmentTransaction ft = getFragmentManager().beginTransaction();

            if(iCounter % 2 == 0) {
                ft.replace(R.id.flFragmentContainer, fragmentTwo);
            } else {
                ft.replace(R.id.flFragmentContainer, fragmentOne);
            }

            iCounter++;
            ft.commit();
        }

        return super.onOptionsItemSelected(item);
    }
}
